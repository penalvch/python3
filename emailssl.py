# Send email via SSL
# Tested working in Python 3.11.6

import smtplib
import socket
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# Email settings
hostname=socket.gethostname()
sender_email=""
receiver_email=""
password=""
subject=""
body=""+hostname
smtpserver=""


message=MIMEMultipart()
message["From"]=sender_email
message["To"]=receiver_email
message["Subject"]=subject
message.attach(MIMEText(body,"Plain"))

try:
    server=smtplib.SMTP_SSL(smtpserver,465)
    server.login(sender_email,password)
    text=message.as_string()
    server.sendmail(sender_email,receiver_email,text)
    # print("DEBUG: Email sent successfully")
except Exception as e:
    print(f"Error: {e}")
finally:
    server.quit()
